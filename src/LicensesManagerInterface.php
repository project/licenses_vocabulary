<?php

namespace Drupal\licenses_vocabulary;

/**
 * Interface to manage licenses terms.
 */
interface LicensesManagerInterface {
  /**
   * Denotes that the license could not be imported, wrong text.
   */
  const LICENSES_VOCABULARY_WRONG_TEXT = -1;

  /**
   * Icon doesn't exist in specified path.
   */
  const LICENSES_VOCABULARY_FILE_NOT_FOUND = -2;

  /**
   * License could not be created.
   */
  const LICENSES_VOCABULARY_TERM_NOT_CREATED = -3;

  /**
   * The license already exists.
   */
  const LICENSES_VOCABULARY_TERM_EXISTS = -4;

  /**
   * Read the list of pre-defined licenses and create each taxonomy terms.
   *
   * @return array
   *   Array with the multiple TermInterface that can be created.
   */
  public function createDefaultLicenses();

  /**
   * Parse text to import licenses from it.
   *
   * @param string $text
   *   The text in YML to import.
   *
   * @return array
   *   Array with the multiple TermInterface that can be created.
   */
  public function createFromText($text);

  /**
   * Read the list of pre-defined licenses.
   *
   * @return array
   *   Array with the multiple default licenses that can be get.
   */
  public function listDefaultLicenses();

  /**
   * Parse text to get licenses from it.
   *
   * @param string $text
   *   The text in YML to get.
   *
   * @return array
   *   Array with the multiple licenses that can be get.
   */
  public function loadFromText($text);

}
