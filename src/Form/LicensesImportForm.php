<?php

namespace Drupal\licenses_vocabulary\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\licenses_vocabulary\LicensesManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to import licenses.
 */
class LicensesImportForm extends ConfigFormBase {
  use StringTranslationTrait;

  /**
   * Licenses manager.
   *
   * @var \Drupal\licenses_vocabulary\LicensesManagerInterface
   */
  protected $licensesManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a new LicensesImportForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LicensesManagerInterface $licenses_manager,
    MessengerInterface $messenger,
    FileUrlGeneratorInterface $file_url_generator,
  ) {
    parent::__construct($config_factory);
    $this->licensesManager = $licenses_manager;
    $this->messenger = $messenger;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('licenses_vocabulary.manager'),
      $container->get('messenger'),
      $container->get('file_url_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'licenses_vocabulary.licensesimport',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'licenses_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['info_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => 'some class',
      ],
    ];

    $info = $this->t('This module adds license as taxonomy terms.');
    $info .= "<br/>";
    $info .= $this->t('Submitting this form will import the licenses specified in below text area.');
    $info .= "<br/>";
    $info .= $this->t('Additionally it will import all licenses from @file.', ['@file' => 'vocabulary_licenses/licenses_vocabulary.services.yml']);
    $info .= "<br/>";
    $tax_url = Link::fromTextAndUrl("taxonomy settings", Url::fromUri("internal:/admin/structure/taxonomy/manage/licenses_vocabulary_licenses/overview"))->toString();
    $info .= $this->t('Press save and it will start the import, then go to @url', ['@url' => $tax_url]);
    $form['info'] = [
      '#type' => 'markup',
      '#markup' => $info,
    ];

    $image_path = substr($this->fileUrlGenerator->generateString('public://licenses_vocabulary/cc'), 1);
    $example_text = "-\n";
    $example_text .= "  title: Attribution 4.0 International (CC BY 4.0)\n";
    $example_text .= "  short_label: CC BY 4.0\n";
    $example_text .= "  icon_file: public://licenses_vocabulary/cc/by.png\n";
    $example_text .= "  url: https://creativecommons.org/licenses/by/4.0/\n";
    $notes = $this->t('Before importing, take care of copying the logos in the correct directory, a good place may be: @image_path', ["@image_path" => $image_path]);
    $notes .= "<br />";
    $notes .= nl2br($this->t('Licenses in the same format as found in vocabulary_licenses/licenses_vocabulary.services.yml . As example:'));

    $form['licenses_text'] = [
      '#title' => $this->t('Licenses text'),
      '#type' => 'textarea',
      '#description' => $notes,
      '#default_value' => '',
      '#rows' => 10,
      '#cols' => 60,
      '#resizable' => TRUE,
    ];
    $form['licenses_example'] = [
      '#type' => 'markup',
      '#markup' => $example_text,
      '#prefix' => '<pre>',
      '#suffix' => '</pre>',
    ];
    $default_licenses = $this->licensesManager->listDefaultLicenses();
    $form['import_default_list'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#title' => $this->t('Default licenses:'),
    ];
    foreach ($default_licenses as $default_license) {
      $form['import_default_list']['#items'][] = $default_license['title'];
    }
    $form['import_default'] = [
      '#type' => $this->t('Import default licenses'),
      '#type' => 'checkbox',
      '#default_value' => FALSE,
      '#title' => $this->t('Import default licenses'),
      '#description' => $this->t('Import also next license list from @file.', ['@file' => 'vocabulary_licenses/licenses_vocabulary.services.yml']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $licenses = [];
    if ($form_state->getValue('licenses_text')) {
      $licenses = $this->licensesManager->createFromText($form_state->getValue('licenses_text'));
    }
    if ($form_state->getValue('import_default')) {
      $licenses = $licenses + $this->licensesManager->createDefaultLicenses();
    }
    if (count($licenses) == 0) {
      $form_state->setErrorByName('licenses_text', $this->t("No license to import."));
    }
    foreach ($licenses as $license) {
      if (is_array($license) && array_key_exists(LicensesManagerInterface::LICENSES_VOCABULARY_WRONG_TEXT, $license)) {
        $form_state->setErrorByName('licenses_text', $license[LicensesManagerInterface::LICENSES_VOCABULARY_WRONG_TEXT]);
      }
      elseif (is_array($license) && array_key_exists(LicensesManagerInterface::LICENSES_VOCABULARY_FILE_NOT_FOUND, $license)) {
        $form_state->setErrorByName('licenses_text', $license[LicensesManagerInterface::LICENSES_VOCABULARY_FILE_NOT_FOUND]);
      }
      elseif (is_array($license) && array_key_exists(LicensesManagerInterface::LICENSES_VOCABULARY_TERM_NOT_CREATED, $license)) {
        $form_state->setErrorByName('licenses_text', $license[LicensesManagerInterface::LICENSES_VOCABULARY_TERM_NOT_CREATED]);
      }
      elseif (is_array($license) && array_key_exists(LicensesManagerInterface::LICENSES_VOCABULARY_TERM_EXISTS, $license)) {
        $this->messenger->addStatus($license[LicensesManagerInterface::LICENSES_VOCABULARY_TERM_EXISTS]);
      }
      else {
        $this->messenger->addStatus($this->t("License: @term created.", ["@term" => $license->toLink()->toString()]));
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('licenses_vocabulary.licensesimport')
      ->save();
  }

}
