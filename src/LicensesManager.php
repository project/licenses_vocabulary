<?php

namespace Drupal\licenses_vocabulary;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;

/**
 * The manager of the licenses.
 */
class LicensesManager implements LicensesManagerInterface {

  use StringTranslationTrait;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \iDrupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Constructs a new LicensesManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, ModuleExtensionList $extension_list_module) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->moduleExtensionList = $extension_list_module;
  }

  /**
   * {@inheritdoc}
   */
  public function loadFromText($text) {
    $license_data = Yaml::decode($text);
    $licenses = [];
    if (is_array($license_data)) {
      foreach ($license_data as $license_item) {
        $keys = ['title', 'short_label', 'icon_file', 'url'];
        if (count(array_diff($keys, array_keys($license_item))) > 0) {
          return [LicensesManagerInterface::LICENSES_VOCABULARY_WRONG_TEXT => $this->t('Some license could not be listed, wrong text')];
        }
        $licenses[] = $license_item;
      }
    }
    return $licenses;
  }

  /**
   * {@inheritdoc}
   */
  public function createFromText($text) {
    $license_data = $this->loadFromText($text);
    $newLicenses = [];
    foreach ($license_data as $license_item) {
      $license_term = $this->createLicenseTerm($license_item['title'], $license_item['short_label'], $license_item['icon_file'], $license_item['url']);
      if ($license_term) {
        $newLicenses[] = $license_term;
      }
    }
    return $newLicenses;
  }

  /**
   * {@inheritdoc}
   */
  public function listDefaultLicenses() {
    $file_path = $this->moduleExtensionList->getPath('licenses_vocabulary') . '/licenses_vocabulary.default.licenses.yml';
    $file_contents = file_get_contents($file_path);
    return $this->loadFromText($file_contents);
  }

  /**
   * {@inheritdoc}
   */
  public function createDefaultLicenses() {
    $file_path = $this->moduleExtensionList->getPath('licenses_vocabulary') . '/licenses_vocabulary.default.licenses.yml';
    $file_contents = file_get_contents($file_path);
    return $this->createFromText($file_contents);
  }

  /**
   * Creates the license if it doesn't already exist.
   *
   * @param string $term_title
   *   The title of the term.
   * @param string $term_short_label
   *   The title of the link.
   * @param string $icon_filename
   *   The filename of the icon of the license.
   * @param string $license_url
   *   The url to the license.
   *
   * @return Drupal\taxonomy\TermInterface|null
   *   The recently created term or null if it exists.
   */
  protected function createLicenseTerm($term_title, $term_short_label, $icon_filename, $license_url) {
    // Check if license already exists.
    $terms = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadByProperties(['name' => $term_title, 'vid' => 'licenses_vocabulary_licenses']);
    if (count($terms) > 0) {
      return [LicensesManagerInterface::LICENSES_VOCABULARY_TERM_EXISTS => $this->t("License @term already exists.", ["@term" => $term_title])];
    }

    // $icon_file_uri = drupal_get_path('module', 'licenses_vocabulary') .
    // "/images/$icon_filename";
    //
    // Just in case the file has already been created.
    // icon_filename was icon_file_uri if image directly from module dir and not
    // using filesystem wrappers.
    $icon_files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $icon_filename]);
    $icon_file = reset($icon_files);

    // If not create a file.
    if (!$icon_file) {
      if (!file_exists($icon_filename)) {
        // $this->messenger->addError($this
        // ->t(
        // 'The license @license could not be created, bad icon path: @file',
        // ['@license' => $term_title, '@file' => $icon_filename]));
        return [
          LicensesManagerInterface::LICENSES_VOCABULARY_FILE_NOT_FOUND => $this->t('The license @license could not be created, bad icon path: @file',
          [
            '@license' => $term_title,
            '@file' => $icon_filename,
          ]),
        ];
      }
      $icon_file = File::create([
        'uri' => $icon_filename,
      ]);
      $icon_file->save();
    }

    $tid = Term::create([
      'name' => $term_title,
      'vid' => 'licenses_vocabulary_licenses',
      'licenses_vocabulary_link' => ['title' => $term_short_label, 'uri' => $license_url],
      'licenses_vocabulary_icon' => [
        'target_id' => $icon_file->id(),
        'alt' => $term_title,
        'title' => $term_title,
      ],
    ]);
    if ($tid->save()) {
      $this->messenger->addStatus($this->t("License: @term created.", ["@term" => $tid->toLink()->toString()]));
      return $tid;
    }
    $this->messenger->addError($this->t('The license @license could not be created', ['@license' => $term_title]));
    return [LicensesManagerInterface::LICENSES_VOCABULARY_TERM_NOT_CREATED => $this->t('The license @license could not be created', ['@license' => $term_title])];
  }

}
