# Licenses vocabulary

This module takes the inspiration from
https://www.drupal.org/project/media_attribution
but is not limited to media.
It just creates the taxonomy so you are free to attach the field to any content
or entity.

It also imports a default set of licenses from the file
licenses_vocabulary.default.licenses.yml.
Additionally it can import licenses from textarea in settings.

Logos are taken from: https://creativecommons.org/about/downloads/ .
